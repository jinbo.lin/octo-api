package client

import (
	"fmt"
	"gitlab.com/dhendel/octo-api/types"
)

type Machines struct {
	client *OctoClient
	*types.Machine
	req *RequestData
}

func (m *Machines) List() ([]Machines, error) {
	machines := make([]Machines, 0)
	m.req.All = true
	m.req.Method = "GET"

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, &machines); err != nil {
		return nil, err
	}

	return machines, nil
}

func (m *Machines) ListByID(machineID string) (*types.Machine, error) {
	machines := &types.Machine{}
	m.req.All = false
	m.req.Method = "GET"
	m.req.ObjectID = machineID

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, machines); err != nil {
		return nil, err
	}

	return machines, nil
}

func (m *Machines) Update(machine *types.Machine) (*types.Machine, error) {

	if machine.ID == "" {
		err := fmt.Errorf("machine ID must be set to update the machine")
		return nil, err
	}

	updateResponse := &types.Machine{}
	m.req.All = false
	m.req.Method = "PUT"

	m.req.ObjectID = machine.ID
	m.req.setBody(machine)

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, updateResponse); err != nil {
		return nil, err
	}

	return updateResponse, err
}
func (m *Machines) Create(machine *types.Machine) (*types.Machine, error) {
	createResponse := &types.Machine{}

	// Do discovery to get the thumbprint
	discovery, err := m.client.Machines.Discover(machine.Name)

	if err != nil {
		return nil, err
	}

	machine.Thumbprint = discovery.Thumbprint

	m.req.Method = "POST"
	// Set to nil and false in case it was previously set for another call
	m.req.ObjectID = ""
	m.req.All = false

	m.req.setBody(machine)

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, createResponse); err != nil {
		return nil, err
	}

	return createResponse, nil
}
func (m *Machines) Delete(machineID string) (*types.DeleteResponse, error) {
	deleteResponse := &types.DeleteResponse{}

	if machineID == "" {
		err := fmt.Errorf("machine ID must be set to delete the machine")
		return nil, err
	}

	m.req.ObjectID = machineID
	m.req.All = false

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, deleteResponse); err != nil {
		return nil, err
	}

	return deleteResponse, nil
}

func (m *Machines) ConnectionStatus(machineID string) (*types.MachineConnectionResponse, error) {
	connectionResponse := &types.MachineConnectionResponse{}
	if machineID == "" {
		err := fmt.Errorf("machine ID must be set to get machine connection status")
		return nil, err
	}

	m.req.Method = "GET"
	m.req.ObjectID = machineID
	m.req.extraPath = "connection"

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, connectionResponse); err != nil {
		return nil, err
	}

	return connectionResponse, nil
}

func (m *Machines) RelatedTasks(machineID string) (*types.MachineTasksResponse, error) {
	tasksResponse := &types.MachineTasksResponse{}
	if machineID == "" {
		err := fmt.Errorf("machine ID must be set to get machine related tasks")
		return nil, err
	}

	m.req.Method = "GET"
	m.req.ObjectID = machineID
	m.req.extraPath = "tasks"

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, tasksResponse); err != nil {
		return nil, err
	}

	return tasksResponse, nil
}

func (m *Machines) Discover(hostname string) (*types.Machine, error) {
	discoverResponse := &types.Machine{}

	if hostname == "" {
		err := fmt.Errorf("hostname must be passed for machine discovery")
		return nil, err
	}

	m.req.Method = "GET"
	m.req.extraPath = "discover"
	m.req.extraParams = map[string]string{
		"host": hostname,
	}

	req, err := m.client.newRequest(m.req)

	if err != nil {
		return nil, err
	}

	if err := m.client.Do(req, discoverResponse); err != nil {
		return nil, err
	}

	return discoverResponse, nil
}
