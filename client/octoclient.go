package client

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

type OctoClient struct {
	hostURL             string
	apiToken            string
	verifySSL           bool
	client              *http.Client
	Machines            *Machines
	Accounts            *Accounts
	Environments        *Environments
	Projects            *Projects
	DeploymentProcesses *DeploymentProcesses
}

type RequestData struct {
	APIEndpoint string
	All         bool
	ObjectID    string
	Method      string
	Body        io.Reader
	extraPath   string
	extraParams map[string]string
}

func NewClient(host, apiToken string, verifySSL bool) *OctoClient {
	oc := &OctoClient{
		hostURL:  host,
		apiToken: apiToken,
		client: &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: verifySSL},
			},
		},
	}

	oc.Machines = &Machines{client: oc,
		req: &RequestData{
			APIEndpoint: "machines",
		},
	}
	oc.Accounts = &Accounts{client: oc,
		req: &RequestData{
			APIEndpoint: "accounts",
		},
	}

	oc.Environments = &Environments{client: oc,
		req: &RequestData{
			APIEndpoint: "environments",
		},
	}

	oc.Projects = &Projects{client: oc,
		req: &RequestData{
			APIEndpoint: "projects",
		},
	}

	oc.DeploymentProcesses = &DeploymentProcesses{client: oc,
		req: &RequestData{
			APIEndpoint: "deploymentprocesses",
		},
	}

	return oc
}

// Do the HTTP Request and unmarshal the response to the passed interface
// This is mainly used to make calls for resources that are not defined
func (o *OctoClient) Do(r *http.Request, v interface{}) error {

	r.Header.Add("X-Octopus-ApiKey", o.apiToken)

	resp, err := o.client.Do(r)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	return json.Unmarshal(respBody, v)

}

func (o *OctoClient) newRequest(reqData *RequestData) (r *http.Request, err error) {

	reqURL := &url.URL{}

	reqURL.Host = o.hostURL
	reqURL.Path = "/api/" + reqData.APIEndpoint
	reqURL.Scheme = "https"

	if reqData.All && reqData.ObjectID != "" {
		err := fmt.Errorf("All and ObjectID cannot be set,you must select one or the other")
		return nil, err
	}

	if reqData.All {
		reqURL.Path = reqURL.Path + "/all"
	}

	if reqData.ObjectID != "" {
		reqURL.Path = reqURL.Path + "/" + reqData.ObjectID
	}

	if reqData.extraPath != "" {
		reqURL.Path = reqURL.Path + "/" + reqData.extraPath
	}

	if reqData.extraParams != nil {
		for k, v := range reqData.extraParams {
			reqURL.Query().Add(k, v)
		}
	}
	log.Info().Msgf("%s", reqURL.String())
	return http.NewRequest(reqData.Method, reqURL.String(), reqData.Body)

}

func (r *RequestData) setBody(i interface{}) {
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(i)
	r.Body = b
}
