package client

import (
	"fmt"
	"gitlab.com/dhendel/octo-api/types"
)

type DeploymentProcesses struct {
	client *OctoClient
	*types.DeploymentProcess
	req *RequestData
}


func (dp *DeploymentProcesses) List() (*types.AllDeploymentProcesses, error) {
	deploymentProcesses := &types.AllDeploymentProcesses{}
	dp.req.All = false
	dp.req.Method = "GET"

	req, err := dp.client.newRequest(dp.req)

	if err != nil {
		return nil, err
	}

	if err := dp.client.Do(req, deploymentProcesses); err != nil {
		return nil, err
	}

	return deploymentProcesses, nil
}

func (dp *DeploymentProcesses) ListByID(deploymentProcessID string) (*types.DeploymentProcess, error) {
	deploymentProcess := &types.DeploymentProcess{}
	dp.req.All = false
	dp.req.Method = "GET"
	dp.req.ObjectID = deploymentProcessID

	req, err := dp.client.newRequest(dp.req)

	if err != nil {
		return nil, err
	}

	if err := dp.client.Do(req, &deploymentProcess); err != nil {
		return nil, err
	}

	return deploymentProcess, nil
}

func (dp *DeploymentProcesses) Update(deploymentProcess *types.DeploymentProcess) (*types.DeploymentProcess, error) {

	if deploymentProcess.ID == "" {
		err := fmt.Errorf("deployment Process ID must be set to update the deployment Process")
		return nil, err
	}

	updateResponse := &types.DeploymentProcess{}
	dp.req.All = false
	dp.req.Method = "PUT"

	dp.req.ObjectID = deploymentProcess.ID
	dp.req.setBody(deploymentProcess)

	req, err := dp.client.newRequest(dp.req)

	if err != nil {
		return nil, err
	}

	if err := dp.client.Do(req, updateResponse); err != nil {
		return nil, err
	}

	return updateResponse, nil
}
