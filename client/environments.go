package client

import (
	"fmt"
	"gitlab.com/dhendel/octo-api/types"
)

type Environments struct {
	client *OctoClient
	*types.Environment
	req *RequestData
}

func (e *Environments) List() ([]types.Environment, error) {
	environments := make([]types.Environment, 0)
	e.req.All = true
	e.req.Method = "GET"

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, &environments); err != nil {
		return nil, err
	}

	return environments, nil
}

func (e *Environments) ListByID(environmentID string) (*types.Environment, error) {
	environment := &types.Environment{}
	e.req.All = false
	e.req.Method = "GET"
	e.req.ObjectID = environmentID

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, environment); err != nil {
		return nil, err
	}

	return environment, nil
}

func (e *Environments) Update(Environment *types.Environment) (*types.Environment, error) {

	if Environment.ID == "" {
		err := fmt.Errorf("environment ID must be set to update the environment")
		return nil, err
	}

	updateResponse := &types.Environment{}
	e.req.All = false
	e.req.Method = "PUT"

	e.req.ObjectID = Environment.ID
	e.req.setBody(Environment)

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, updateResponse); err != nil {
		return nil, err
	}

	return updateResponse, err
}
func (e *Environments) Create(Environment *types.Environment) (*types.Environment, error) {
	createResponse := &types.Environment{}
	e.req.Method = "POST"

	e.req.ObjectID = ""
	e.req.All = false

	e.req.setBody(Environment)

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, createResponse); err != nil {
		return nil, err
	}

	return createResponse, nil
}
func (e *Environments) Delete(environmentID string) (*types.DeleteResponse, error) {
	deleteResponse := &types.DeleteResponse{}

	if environmentID == "" {
		err := fmt.Errorf("environment ID must be set to delete the environment")
		return nil, err
	}
	e.req.Method = "DELETE"
	e.req.ObjectID = environmentID
	e.req.All = false

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, deleteResponse); err != nil {
		return nil, err
	}

	return deleteResponse, nil
}

func (e *Environments) Summary() (*types.EnvironmentSummaryResponse, error) {
	Environments := &types.EnvironmentSummaryResponse{}
	e.req.All = false
	e.req.Method = "GET"
	e.req.ObjectID = ""

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, Environments); err != nil {
		return nil, err
	}

	return Environments, nil
}

func (e *Environments) ListEnvironmentMachines(environmentID string) ([]types.EnvironmentMachinesResponse, error) {
	environmentMachines := make([]types.EnvironmentMachinesResponse, 0)

	if environmentID == "" {
		err := fmt.Errorf("environment ID must be get to machines for the environment")
		return nil, err
	}

	e.req.All = false
	e.req.Method = "GET"
	e.req.ObjectID = environmentID
	e.req.extraPath = "machines"

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, environmentMachines); err != nil {
		return nil, err
	}

	return environmentMachines, nil
}

// ListSingleScopedVariables Lists all the variable set names (projects and library variable sets) that have variables that are scoped to only the given environment
func (e *Environments) ListSingleScopedVariables(environmentID string) (*types.SingularlyScopedVariablesResponse, error) {
	environmentMachines := &types.SingularlyScopedVariablesResponse{}

	if environmentID == "" {
		err := fmt.Errorf("environment ID must be set to get single scoped variables the environment")
		return nil, err
	}

	e.req.All = false
	e.req.Method = "GET"
	e.req.ObjectID = environmentID
	e.req.extraPath = "singlyScopedVariableDetails"

	req, err := e.client.newRequest(e.req)

	if err != nil {
		return nil, err
	}

	if err := e.client.Do(req, environmentMachines); err != nil {
		return nil, err
	}

	return environmentMachines, nil
}
