# octo-api

An octopus api golang client

# Using the library

Supported API's:

- Machines
- Environments
- Projects
- Accounts

Each api endpoint has the following basic methods: `List`, `Create`, `Update`, `Delete`

In addition to those methods environments supports a few extra calls

### Basic usage

```go
package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/dhendel/octo-api/client"
)

func main() {

    octoURL := "demo.octopus.com"
    apiToken := "TESTASJDLKJDS-TOKEN"
    
	octo := client.NewClient(octoURL, apiToken, false)

	envs, err := octo.Environments.List()

	if err != nil {
		log.Fatal().Err(err).Msg("Unable to get list of all environments")
	}

	roles, err := octo.Projects.List()

	if err != nil {
		log.Fatal().Err(err).Msg("Unable to get list of all roles")
	}
	
	...
}

```

### Executing a call that is not currently Supported

```go

package main

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/dhendel/octo-api/client"
	"net/http"
	"fmt"
)

func main() {

    octoURL := "demo.octopus.com"
    apiToken := "TESTASJDLKJDS-TOKEN"
    
    octo := client.NewClient(octoURL, apiToken, false)

    unsuportedAPIPath := fmt.Sprintf("%s/api/UNSUPPORTED_CALL", octoURL)

    newReq, err := http.NewRequest("GET", unsuportedAPIPath, nil)
    
    if err != nil {
        log.Fatal().Err(err).Msg("Failed request")
    }
    
    respinterface := &MyInterface{}
    
    if err := octo.Do(newReq, respinterface); err != nil {
        log.Fatal().Err(err).Msg("Failed request")
    }
    
    log.Info().Msgf("%+v", respinterface)
	
    ...
}

```
