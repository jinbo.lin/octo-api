package types

import "time"

type Environment struct {
	ID                         string    `json:"Id"`
	Name                       string    `json:"Name"`
	Description                string    `json:"Description"`
	SortOrder                  int       `json:"SortOrder"`
	UseGuidedFailure           bool      `json:"UseGuidedFailure"`
	AllowDynamicInfrastructure bool      `json:"AllowDynamicInfrastructure"`
	LastModifiedOn             time.Time `json:"LastModifiedOn"`
	LastModifiedBy             string    `json:"LastModifiedBy"`
	Links                      struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}

type EnvironmentSummaryResponse struct {
	TotalMachines                int `json:"TotalMachines"`
	TotalDisabledMachines        int `json:"TotalDisabledMachines"`
	MachineHealthStatusSummaries struct {
		Healthy     int `json:"Healthy"`
		Unavailable int `json:"Unavailable"`
		Unknown     int `json:"Unknown"`
		HasWarnings int `json:"HasWarnings"`
		Unhealthy   int `json:"Unhealthy"`
	} `json:"MachineHealthStatusSummaries"`
	MachineEndpointSummaries struct {
		None                      int `json:"None"`
		TentaclePassive           int `json:"TentaclePassive"`
		TentacleActive            int `json:"TentacleActive"`
		SSH                       int `json:"Ssh"`
		OfflineDrop               int `json:"OfflineDrop"`
		AzureWebApp               int `json:"AzureWebApp"`
		Ftp                       int `json:"Ftp"`
		AzureCloudService         int `json:"AzureCloudService"`
		AzureServiceFabricCluster int `json:"AzureServiceFabricCluster"`
		Kubernetes                int `json:"Kubernetes"`
	} `json:"MachineEndpointSummaries"`
	MachineTenantSummaries struct {
		Tenants21 int `json:"Tenants-21"`
	} `json:"MachineTenantSummaries"`
	MachineTenantTagSummaries struct {
	} `json:"MachineTenantTagSummaries"`
	EnvironmentSummaries []struct {
		*Environment
		TotalMachines                int `json:"TotalMachines"`
		TotalDisabledMachines        int `json:"TotalDisabledMachines"`
		MachineHealthStatusSummaries struct {
			Healthy     int `json:"Healthy"`
			Unavailable int `json:"Unavailable"`
			Unknown     int `json:"Unknown"`
			HasWarnings int `json:"HasWarnings"`
			Unhealthy   int `json:"Unhealthy"`
		} `json:"MachineHealthStatusSummaries"`
		MachineEndpointSummaries struct {
			None                      int `json:"None"`
			TentaclePassive           int `json:"TentaclePassive"`
			TentacleActive            int `json:"TentacleActive"`
			SSH                       int `json:"Ssh"`
			OfflineDrop               int `json:"OfflineDrop"`
			AzureWebApp               int `json:"AzureWebApp"`
			Ftp                       int `json:"Ftp"`
			AzureCloudService         int `json:"AzureCloudService"`
			AzureServiceFabricCluster int `json:"AzureServiceFabricCluster"`
			Kubernetes                int `json:"Kubernetes"`
		} `json:"MachineEndpointSummaries"`
		MachineTenantSummaries struct {
			Tenants21 int `json:"Tenants-21"`
		} `json:"MachineTenantSummaries"`
		MachineTenantTagSummaries struct {
		} `json:"MachineTenantTagSummaries"`
		TentacleUpgradesRequired     bool          `json:"TentacleUpgradesRequired"`
		MachineIdsForCalamariUpgrade []interface{} `json:"MachineIdsForCalamariUpgrade"`
	} `json:"EnvironmentSummaries"`
	TentacleUpgradesRequired     bool     `json:"TentacleUpgradesRequired"`
	MachineIdsForCalamariUpgrade []string `json:"MachineIdsForCalamariUpgrade"`
}

type SingularlyScopedVariablesResponse struct {
	HasUnauthorizedProjectVariables            bool `json:"HasUnauthorizedProjectVariables"`
	HasUnauthorizedLibraryVariableSetVariables bool `json:"HasUnauthorizedLibraryVariableSetVariables"`
	VariableMap                                struct {
		AdditionalProp1 struct {
			AdditionalProp1 int `json:"additionalProp1"`
			AdditionalProp2 int `json:"additionalProp2"`
			AdditionalProp3 int `json:"additionalProp3"`
		} `json:"additionalProp1"`
		AdditionalProp2 struct {
			AdditionalProp1 int `json:"additionalProp1"`
			AdditionalProp2 int `json:"additionalProp2"`
			AdditionalProp3 int `json:"additionalProp3"`
		} `json:"additionalProp2"`
		AdditionalProp3 struct {
			AdditionalProp1 int `json:"additionalProp1"`
			AdditionalProp2 int `json:"additionalProp2"`
			AdditionalProp3 int `json:"additionalProp3"`
		} `json:"additionalProp3"`
	} `json:"VariableMap"`
}

type EnvironmentMachinesResponse struct {
	ItemType       string `json:"ItemType"`
	TotalResults   int    `json:"TotalResults"`
	ItemsPerPage   int    `json:"ItemsPerPage"`
	NumberOfPages  int    `json:"NumberOfPages"`
	LastPageNumber int    `json:"LastPageNumber"`
	Items          []struct {
		ID                              string        `json:"Id"`
		EnvironmentIds                  []string      `json:"EnvironmentIds"`
		Roles                           []string      `json:"Roles"`
		TenantedDeploymentParticipation string        `json:"TenantedDeploymentParticipation"`
		TenantIds                       []interface{} `json:"TenantIds"`
		TenantTags                      []interface{} `json:"TenantTags"`
		Name                            string        `json:"Name"`
		Thumbprint                      interface{}   `json:"Thumbprint"`
		URI                             interface{}   `json:"Uri"`
		IsDisabled                      bool          `json:"IsDisabled"`
		MachinePolicyID                 string        `json:"MachinePolicyId"`
		Status                          string        `json:"Status"`
		HealthStatus                    string        `json:"HealthStatus"`
		HasLatestCalamari               bool          `json:"HasLatestCalamari"`
		StatusSummary                   string        `json:"StatusSummary"`
		IsInProcess                     bool          `json:"IsInProcess"`
		Endpoint                        struct {
			CommunicationStyle string      `json:"CommunicationStyle"`
			AccountID          string      `json:"AccountId"`
			Host               string      `json:"Host"`
			Port               int         `json:"Port"`
			Fingerprint        string      `json:"Fingerprint"`
			URI                string      `json:"Uri"`
			ProxyID            interface{} `json:"ProxyId"`
			DotNetCorePlatform string      `json:"DotNetCorePlatform"`
			ID                 interface{} `json:"Id"`
			LastModifiedOn     interface{} `json:"LastModifiedOn"`
			LastModifiedBy     interface{} `json:"LastModifiedBy"`
			Links              struct {
			} `json:"Links"`
		} `json:"Endpoint"`
		Links struct {
			Self          string `json:"Self"`
			Connection    string `json:"Connection"`
			TasksTemplate string `json:"TasksTemplate"`
		} `json:"Links"`
	} `json:"Items"`
	Links struct {
		Self        string `json:"Self"`
		Template    string `json:"Template"`
		PageAll     string `json:"Page.All"`
		PageCurrent string `json:"Page.Current"`
		PageLast    string `json:"Page.Last"`
	} `json:"Links"`
}
