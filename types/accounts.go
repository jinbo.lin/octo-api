package types

import "time"

// AllAccountResponse stores a list of all accounts frm /api/accounts/all
type AllAccountResponse []Account

// Account is the base structure for AccountResource
type Account struct {
	ID                              string    `json:"Id"`
	Name                            string    `json:"Name"`
	Description                     string    `json:"Description"`
	EnvironmentIds                  []string  `json:"EnvironmentIds"`
	TenantedDeploymentParticipation string    `json:"TenantedDeploymentParticipation"`
	TenantIds                       []string  `json:"TenantIds"`
	TenantTags                      []string  `json:"TenantTags"`
	AccountType                     string    `json:"AccountType"`
	LastModifiedOn                  time.Time `json:"LastModifiedOn"`
	LastModifiedBy                  string    `json:"LastModifiedBy"`
	Links                           struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}

// DeleteAccountRequest ID is a path parameter
type AccountActionByIDRequest struct {
	ID string `json:"id"`
}

// AccountResource Account Model
type AccountResource struct {
	ID             string `json:"Id"`
	ItemType       string `json:"ItemType"`
	TotalResults   int    `json:"TotalResults"`
	ItemsPerPage   int    `json:"ItemsPerPage"`
	NumberOfPages  int    `json:"NumberOfPages"`
	LastPageNumber int    `json:"LastPageNumber"`
	Items          []struct {
		*Account
	}
	LastModifiedOn time.Time `json:"LastModifiedOn"`
	LastModifiedBy string    `json:"LastModifiedBy"`
	Links          struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}
