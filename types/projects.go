package types

import "time"

type Project struct {
	ID                              string   `json:"Id"`
	VariableSetID                   string   `json:"VariableSetId"`
	DeploymentProcessID             string   `json:"DeploymentProcessId"`
	DiscreteChannelRelease          bool     `json:"DiscreteChannelRelease"`
	IncludedLibraryVariableSetIds   []string `json:"IncludedLibraryVariableSetIds"`
	DefaultToSkipIfAlreadyInstalled bool     `json:"DefaultToSkipIfAlreadyInstalled"`
	TenantedDeploymentMode          string   `json:"TenantedDeploymentMode"`
	DefaultGuidedFailureMode        string   `json:"DefaultGuidedFailureMode"`
	VersioningStrategy              struct {
		DonorPackageStepID string `json:"DonorPackageStepId"`
		Template           string `json:"Template"`
	} `json:"VersioningStrategy"`
	ReleaseCreationStrategy struct {
		ReleaseCreationPackageStepID string `json:"ReleaseCreationPackageStepId"`
		ChannelID                    string `json:"ChannelId"`
	} `json:"ReleaseCreationStrategy"`
	Templates []struct {
		ID           string `json:"Id"`
		Name         string `json:"Name"`
		Label        string `json:"Label"`
		HelpText     string `json:"HelpText"`
		DefaultValue struct {
			IsSensitive    bool   `json:"IsSensitive"`
			Value          string `json:"Value"`
			SensitiveValue struct {
				HasValue bool   `json:"HasValue"`
				NewValue string `json:"NewValue"`
			} `json:"SensitiveValue"`
		} `json:"DefaultValue"`
		DisplaySettings struct {
			AdditionalProp1 string `json:"additionalProp1"`
			AdditionalProp2 string `json:"additionalProp2"`
			AdditionalProp3 string `json:"additionalProp3"`
		} `json:"DisplaySettings"`
	} `json:"Templates"`
	AutoDeployReleaseOverrides []struct {
		EnvironmentID string `json:"EnvironmentId"`
		TenantID      string `json:"TenantId"`
		ReleaseID     string `json:"ReleaseId"`
	} `json:"AutoDeployReleaseOverrides"`
	Name                      string `json:"Name"`
	Slug                      string `json:"Slug"`
	Description               string `json:"Description"`
	IsDisabled                bool   `json:"IsDisabled"`
	ProjectGroupID            string `json:"ProjectGroupId"`
	LifecycleID               string `json:"LifecycleId"`
	AutoCreateRelease         bool   `json:"AutoCreateRelease"`
	ProjectConnectivityPolicy struct {
		SkipMachineBehavior         string   `json:"SkipMachineBehavior"`
		TargetRoles                 []string `json:"TargetRoles"`
		AllowDeploymentsToNoTargets bool     `json:"AllowDeploymentsToNoTargets"`
	} `json:"ProjectConnectivityPolicy"`
	LastModifiedOn time.Time `json:"LastModifiedOn"`
	LastModifiedBy string    `json:"LastModifiedBy"`
	Links          struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}
