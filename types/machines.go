package types

import (
	"time"
)

type AllMachines []string

type Machine struct {
	ID                              string   `json:"Id"`
	EnvironmentIds                  []string `json:"EnvironmentIds"`
	Roles                           []string `json:"Roles"`
	TenantedDeploymentParticipation string   `json:"TenantedDeploymentParticipation"`
	TenantIds                       []string `json:"TenantIds"`
	TenantTags                      []string `json:"TenantTags"`
	Name                            string   `json:"Name"`
	Thumbprint                      string   `json:"Thumbprint"`
	URI                             string   `json:"Uri"`
	IsDisabled                      bool     `json:"IsDisabled"`
	MachinePolicyID                 string   `json:"MachinePolicyId"`
	Status                          string   `json:"Status"`
	HealthStatus                    string   `json:"HealthStatus"`
	HasLatestCalamari               bool     `json:"HasLatestCalamari"`
	StatusSummary                   string   `json:"StatusSummary"`
	IsInProcess                     bool     `json:"IsInProcess"`
	Endpoint                        struct {
		ID             string    `json:"Id"`
		LastModifiedOn time.Time `json:"LastModifiedOn"`
		LastModifiedBy string    `json:"LastModifiedBy"`
		Links          struct {
			AdditionalProp1 string `json:"additionalProp1"`
			AdditionalProp2 string `json:"additionalProp2"`
			AdditionalProp3 string `json:"additionalProp3"`
		} `json:"Links"`
	} `json:"Endpoint"`
	LastModifiedOn time.Time `json:"LastModifiedOn"`
	LastModifiedBy string    `json:"LastModifiedBy"`
	Links          struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}

type MachineConnectionResponse struct {
	ID        string `json:"Id"`
	MachineID string `json:"MachineId"`
	Logs      []struct {
		Category    string    `json:"Category"`
		OccurredAt  time.Time `json:"OccurredAt"`
		MessageText string    `json:"MessageText"`
		Detail      string    `json:"Detail"`
	} `json:"Logs"`
	Status                 string    `json:"Status"`
	CurrentTentacleVersion string    `json:"CurrentTentacleVersion"`
	LastChecked            time.Time `json:"LastChecked"`
	LastModifiedOn         time.Time `json:"LastModifiedOn"`
	LastModifiedBy         string    `json:"LastModifiedBy"`
	Links                  struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}

type MachineTasksResponse struct {
	ID             string `json:"Id"`
	ItemType       string `json:"ItemType"`
	TotalResults   int    `json:"TotalResults"`
	ItemsPerPage   int    `json:"ItemsPerPage"`
	NumberOfPages  int    `json:"NumberOfPages"`
	LastPageNumber int    `json:"LastPageNumber"`
	Items          []struct {
		ID          string `json:"Id"`
		Name        string `json:"Name"`
		Description string `json:"Description"`
		Arguments   struct {
			AdditionalProp1 struct {
			} `json:"additionalProp1"`
			AdditionalProp2 struct {
			} `json:"additionalProp2"`
			AdditionalProp3 struct {
			} `json:"additionalProp3"`
		} `json:"Arguments"`
		State                      string    `json:"State"`
		Completed                  string    `json:"Completed"`
		QueueTime                  time.Time `json:"QueueTime"`
		QueueTimeExpiry            time.Time `json:"QueueTimeExpiry"`
		StartTime                  time.Time `json:"StartTime"`
		LastUpdatedTime            time.Time `json:"LastUpdatedTime"`
		CompletedTime              time.Time `json:"CompletedTime"`
		ServerNode                 string    `json:"ServerNode"`
		Duration                   string    `json:"Duration"`
		ErrorMessage               string    `json:"ErrorMessage"`
		HasBeenPickedUpByProcessor bool      `json:"HasBeenPickedUpByProcessor"`
		IsCompleted                bool      `json:"IsCompleted"`
		FinishedSuccessfully       bool      `json:"FinishedSuccessfully"`
		HasPendingInterruptions    bool      `json:"HasPendingInterruptions"`
		CanRerun                   bool      `json:"CanRerun"`
		HasWarningsOrErrors        bool      `json:"HasWarningsOrErrors"`
		LastModifiedOn             time.Time `json:"LastModifiedOn"`
		LastModifiedBy             string    `json:"LastModifiedBy"`
		Links                      struct {
			AdditionalProp1 string `json:"additionalProp1"`
			AdditionalProp2 string `json:"additionalProp2"`
			AdditionalProp3 string `json:"additionalProp3"`
		} `json:"Links"`
	} `json:"Items"`
	LastModifiedOn time.Time `json:"LastModifiedOn"`
	LastModifiedBy string    `json:"LastModifiedBy"`
	Links          struct {
		AdditionalProp1 string `json:"additionalProp1"`
		AdditionalProp2 string `json:"additionalProp2"`
		AdditionalProp3 string `json:"additionalProp3"`
	} `json:"Links"`
}
